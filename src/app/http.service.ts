import {Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
 
 
@Injectable()
export class apiService {
 
    constructor(private http:HttpClient) {}
    addPost(data) { // add a new post
        return this.http.post('https://jsonplaceholder.typicode.com/posts',data);
        
    }
    getAllPost(page){ //get all post list
        return this.http.get('https://jsonplaceholder.typicode.com/posts?_page='+page+'&_limit=10')
    }
    deletePost(id){ //delete a post
        return this.http.delete('https://jsonplaceholder.typicode.com/posts/'+id)
    }
    editpost(id,data){ //edit  post
        return this.http.put('https://jsonplaceholder.typicode.com/posts/'+id,JSON.stringify(data))
    }
}