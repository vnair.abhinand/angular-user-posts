import { Component, OnInit } from '@angular/core';
import { apiService } from './http.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder, Validators, AbstractControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public isEdit = false; //flag for edit/add user
  public post_list;
  public page = 1
  public edit_id;
  public length;
  public addPostForm: FormGroup; // add user/edit user form
  constructor(private _apiService: apiService, private modalService: NgbModal, private _fb: FormBuilder) {
  }
  closeResult: string;
  ngOnInit() {
    this.getData();
    this.addPostForm = this._fb.group({
      title: ['', [<any>Validators.required]],
      body: ['', [<any>Validators.required],],
      user_id: ['', [<any>Validators.required],]
    });

  }

  getData() {
    console.log(this.page)
    this._apiService.getAllPost(this.page).subscribe(responce => {
      console.log(responce)
      this.post_list = responce
      this.length = this.post_list.length;
    })
  }
//open modal for edit a new post
  add(modalId) {
    this.addPostForm.reset()
    this.isEdit = false;
    this.modalService.open(modalId)
  }
  //open modal for edit post
  edit(modalId, data) {
    this.edit_id = data.id
    this.isEdit = true
    this.addPostForm.controls.title.setValue(data.title)
    this.addPostForm.controls.body.setValue(data.body)
    this.addPostForm.controls.user_id.setValue(data.userId)
    this.addPostForm.value.title = data.title;
    this.modalService.open(modalId)
  }
  //function exicte when user click add/edit  post
  addPost(data) { 
    if (!this.isEdit) {
      
      this._apiService.addPost(data).subscribe(res => {
        this.modalService.dismissAll()
        this.getData()
      })
    }
    else {
      this._apiService.editpost( this.edit_id,data).subscribe(res => {
        this.modalService.dismissAll()
        this.getData()
      })
    }
  }
    //function exicte when user click delete post
  delete(ModalId, id) {
    this.modalService.open(ModalId).result.then((result) => {
      if (result) {
        this._apiService.deletePost(id).subscribe(data => {
          this.getData()
        })
      }
    })
  }


}
