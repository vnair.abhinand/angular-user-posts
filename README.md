# angular-user-posts

Sample angular application with basic CURD operations

## Installation

Use the package manager npm to install angular-user-posts.

```bash
npm install
```

## Usage

```bash
ng serve
```
